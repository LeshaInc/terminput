use std::fmt::{self, Formatter};

use crate::{Key, Modifiers};

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum KeyFormat {
    Long,
    Short,
    LongCapitalized,
    ShortCapitalized,

    #[doc(hidden)]
    __Nonexhaustive,
}

impl KeyFormat {
    pub fn is_capitalized(self) -> bool {
        match self {
            KeyFormat::LongCapitalized => true,
            KeyFormat::ShortCapitalized => true,
            _ => false,
        }
    }
}

impl Default for KeyFormat {
    fn default() -> KeyFormat {
        KeyFormat::LongCapitalized
    }
}

macro_rules! fmt_modifiers {
    ($mods:expr, $ctrl:literal, $alt:literal, $shift:literal) => {
        match $mods.bits() {
            0b001 => $shift,
            0b010 => $alt,
            0b011 => concat!($alt, "-", $shift),
            0b100 => $ctrl,
            0b101 => concat!($ctrl, "-", $shift),
            0b110 => concat!($ctrl, "-", $alt),
            0b111 => concat!($ctrl, "-", $alt, "-", $shift),
            _ => "",
        }
    };
}

fn fmt_key_long(f: &mut Formatter<'_>, key: Key) -> fmt::Result {
    if let Key::Char(c) = key {
        return write!(f, "{}", c);
    }

    if let Key::Function(n) = key {
        return write!(f, "f{}", n);
    }

    f.write_str(match key {
        Key::Up => "up",
        Key::Down => "down",
        Key::Left => "left",
        Key::Right => "right",
        Key::Begin => "begin",
        Key::Escape => "escape",
        Key::Tab => "tab",
        Key::Find => "find",
        Key::Insert => "insert",
        Key::Delete => "delete",
        Key::Select => "select",
        Key::PageUp => "pageup",
        Key::PageDown => "pagedown",
        Key::Home => "home",
        Key::End => "end",
        Key::Enter => "enter",
        _ => return Ok(()),
    })
}

fn fmt_key_long_cap(f: &mut Formatter<'_>, key: Key) -> fmt::Result {
    if let Key::Char(c) = key {
        return write!(f, "{}", c);
    }

    if let Key::Function(n) = key {
        return write!(f, "F{}", n);
    }

    f.write_str(match key {
        Key::Up => "Up",
        Key::Down => "Down",
        Key::Left => "Left",
        Key::Right => "Right",
        Key::Begin => "Begin",
        Key::Escape => "Escape",
        Key::Tab => "Tab",
        Key::Find => "Find",
        Key::Insert => "Insert",
        Key::Delete => "Delete",
        Key::Select => "Select",
        Key::PageUp => "PageUp",
        Key::PageDown => "PageDown",
        Key::Home => "Home",
        Key::End => "End",
        Key::Enter => "Enter",
        _ => return Ok(()),
    })
}

fn fmt_key_short(f: &mut Formatter<'_>, key: Key) -> fmt::Result {
    if let Key::Char(c) = key {
        return write!(f, "{}", c);
    }

    if let Key::Function(n) = key {
        return write!(f, "f{}", n);
    }

    f.write_str(match key {
        Key::Up => "up",
        Key::Down => "down",
        Key::Left => "left",
        Key::Right => "right",
        Key::Begin => "begin",
        Key::Escape => "esc",
        Key::Tab => "tab",
        Key::Find => "find",
        Key::Insert => "ins",
        Key::Delete => "del",
        Key::Select => "sel",
        Key::PageUp => "pgup",
        Key::PageDown => "pgdown",
        Key::Home => "home",
        Key::End => "end",
        Key::Enter => "enter",
        _ => return Ok(()),
    })
}

fn fmt_key_short_cap(f: &mut Formatter<'_>, key: Key) -> fmt::Result {
    if let Key::Char(c) = key {
        return write!(f, "{}", c);
    }

    if let Key::Function(n) = key {
        return write!(f, "F{}", n);
    }

    f.write_str(match key {
        Key::Up => "Up",
        Key::Down => "Down",
        Key::Left => "Left",
        Key::Right => "Right",
        Key::Begin => "Begin",
        Key::Escape => "Esc",
        Key::Tab => "Tab",
        Key::Find => "Find",
        Key::Insert => "Ins",
        Key::Delete => "Del",
        Key::Select => "Sel",
        Key::PageUp => "PgUp",
        Key::PageDown => "PgDown",
        Key::Home => "Home",
        Key::End => "End",
        Key::Enter => "Enter",
        _ => return Ok(()),
    })
}

impl KeyFormat {
    pub(crate) fn modifiers_as_str(self, mods: Modifiers) -> &'static str {
        match self {
            KeyFormat::Long => fmt_modifiers!(mods, "ctrl", "alt", "shift"),
            KeyFormat::LongCapitalized => fmt_modifiers!(mods, "Ctrl", "Alt", "Shift"),
            KeyFormat::Short => fmt_modifiers!(mods, "c", "a", "s"),
            KeyFormat::ShortCapitalized => fmt_modifiers!(mods, "C", "A", "S"),
            _ => panic!("KeyFormat::__Nonexhaustive must not be used"),
        }
    }

    pub(crate) fn format_key(self, f: &mut Formatter<'_>, key: Key) -> fmt::Result {
        match self {
            KeyFormat::Long => fmt_key_long(f, key),
            KeyFormat::LongCapitalized => fmt_key_long_cap(f, key),
            KeyFormat::Short => fmt_key_short(f, key),
            KeyFormat::ShortCapitalized => fmt_key_short_cap(f, key),
            _ => panic!("KeyFormat::__Nonexhaustive must not be used"),
        }
    }
}
