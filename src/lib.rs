use std::fmt::{self, Display, Formatter};

mod driver;
mod format;

pub use self::format::KeyFormat;

bitflags::bitflags! {
    #[derive(Default)]
    pub struct Modifiers: u32 {
        const EMPTY = 0;
        const SHIFT = 1;
        const ALT = 1 << 1;
        const CTRL = 1 << 2;
    }
}

impl Modifiers {
    #[inline]
    pub fn from_csi(value: u8) -> Modifiers {
        Modifiers::from_bits_truncate(u32::from(value).saturating_sub(1))
    }

    #[inline]
    pub fn into_csi(self) -> u8 {
        (self.bits() as u8) + 1
    }

    pub fn as_str(self, format: KeyFormat) -> &'static str {
        format.modifiers_as_str(self)
    }

    pub fn format(self, format: KeyFormat) -> ModifiersFormatter {
        ModifiersFormatter {
            modifiers: self,
            format,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct ModifiersFormatter {
    modifiers: Modifiers,
    format: KeyFormat,
}

impl Display for ModifiersFormatter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(self.modifiers.as_str(self.format))
    }
}

impl Display for Modifiers {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(self.as_str(KeyFormat::default()))
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Key {
    Char(char),
    Function(u8),
    Up,
    Down,
    Left,
    Right,
    Begin,
    Escape,
    Tab,
    Find,
    Insert,
    Delete,
    Select,
    PageUp,
    PageDown,
    Home,
    End,
    Enter,

    #[doc(hidden)]
    __Nonexhaustive,
}

impl Key {
    pub fn format(self, format: KeyFormat) -> KeyFormatter {
        KeyFormatter { key: self, format }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct KeyFormatter {
    key: Key,
    format: KeyFormat,
}

impl Display for KeyFormatter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.format.format_key(f, self.key)
    }
}

impl Display for Key {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        KeyFormat::default().format_key(f, *self)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct KeyboardInput {
    pub key: Key,
    pub modifiers: Modifiers,
}

impl KeyboardInput {
    pub fn format(self, format: KeyFormat) -> KeyboardInputFormatter {
        KeyboardInputFormatter {
            input: self,
            format,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct KeyboardInputFormatter {
    input: KeyboardInput,
    format: KeyFormat,
}

impl Display for KeyboardInputFormatter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if !self.input.modifiers.is_empty() {
            self.input.modifiers.format(self.format).fmt(f)?;
            f.write_str("-")?;
        }

        if self.format.is_capitalized() && !self.input.modifiers.is_empty() {
            if let Key::Char(c) = self.input.key {
                return write!(f, "{}", c.to_ascii_uppercase());
            }
        }

        self.input.key.format(self.format).fmt(f)
    }
}

impl Display for KeyboardInput {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.format(KeyFormat::default()).fmt(f)
    }
}

impl From<Key> for KeyboardInput {
    fn from(key: Key) -> KeyboardInput {
        KeyboardInput {
            key,
            modifiers: Modifiers::EMPTY,
        }
    }
}

impl From<(Key, Modifiers)> for KeyboardInput {
    fn from((key, modifiers): (Key, Modifiers)) -> KeyboardInput {
        KeyboardInput { key, modifiers }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Input {
    Keyboard(KeyboardInput),
}

impl Input {
    pub fn get_modifiers_mut(&mut self) -> &mut Modifiers {
        match self {
            Input::Keyboard(KeyboardInput {
                ref mut modifiers, ..
            }) => modifiers,
        }
    }

    pub fn format(self, format: KeyFormat) -> InputFormatter {
        InputFormatter {
            input: self,
            format,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct InputFormatter {
    input: Input,
    format: KeyFormat,
}

impl Display for InputFormatter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.input {
            Input::Keyboard(i) => i.format(self.format).fmt(f),
        }
    }
}

impl Display for Input {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.format(KeyFormat::default()).fmt(f)
    }
}

impl From<Key> for Input {
    fn from(key: Key) -> Input {
        Input::Keyboard(KeyboardInput {
            key,
            modifiers: Modifiers::EMPTY,
        })
    }
}

impl From<(Key, Modifiers)> for Input {
    fn from((key, modifiers): (Key, Modifiers)) -> Input {
        Input::Keyboard(KeyboardInput { key, modifiers })
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum ParseError {
    UnknownSequence,
    Incomplete,
}

pub type ParseResult<'a, T> = Result<(&'a [u8], T), ParseError>;

pub fn parse(input: &[u8]) -> ParseResult<'_, Input> {
    self::driver::parse(input)
}
