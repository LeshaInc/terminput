mod csi;
mod simple;

use crate::{Input, Modifiers, ParseResult};

fn try_all(input: &[u8]) -> ParseResult<'_, Input> {
    self::csi::parse(input).or_else(|_| self::simple::parse(input))
}

pub fn parse(input: &[u8]) -> ParseResult<'_, Input> {
    if input.starts_with(b"\x1B") && input.len() > 1 {
        // might be an alt+key sequence
        let (rest, key) = try_all(input)?;
        if rest == &input[1..] {
            // yep
            try_all(&input[1..]).map(|mut k| {
                *k.1.get_modifiers_mut() |= Modifiers::ALT;
                k
            })
        } else {
            // nope
            Ok((rest, key))
        }
    } else {
        try_all(input)
    }
}
