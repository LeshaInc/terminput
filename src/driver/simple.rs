use crate::{Input, Key, Modifiers, ParseError, ParseResult};

// C0 control codes: ctrl + char
fn match_c0(mut code: u8) -> Input {
    debug_assert!(code < 0x20);

    match code {
        0x09 => return Key::Tab.into(),
        0x0D => return Key::Enter.into(),
        0x1B => return Key::Escape.into(),
        _ => (),
    }

    code += 0x40;

    if code >= b'A' && code <= b'Z' {
        code += 0x20;
    }

    (Key::Char(char::from(code)), Modifiers::CTRL).into()
}

pub fn parse(input: &[u8]) -> ParseResult<'_, Input> {
    if input.is_empty() {
        return Err(ParseError::Incomplete);
    }

    match input[0] {
        // C0 range
        c if c < 0x20 => Ok((&input[1..], match_c0(c))),

        // ASCII printable characters
        c if c < 0x7F => Ok((&input[1..], Key::Char(char::from(c)).into())),

        // ASCII DEL
        0x7F => Ok((&input[1..], Key::Delete.into())),

        // might be an UTF-8 character
        _ => match std::str::from_utf8(&input[..4.min(input.len())]) {
            Ok(str) => {
                let char = str.chars().next().unwrap();
                Ok((&input[char.len_utf8()..], Key::Char(char).into()))
            }

            Err(_) => Err(ParseError::UnknownSequence),
        },
    }
}
