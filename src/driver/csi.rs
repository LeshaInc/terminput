use std::char;
use std::num::NonZeroU32;

use arrayvec::ArrayVec;

use crate::{Input, Key, Modifiers, ParseError, ParseResult};

fn begin_escape_prefixed(input: &[u8]) -> ParseResult<'_, Input> {
    if input.is_empty() {
        return Err(ParseError::Incomplete);
    }

    let rest = &input[1..];

    match input[0] {
        // SS3
        0x4f => begin_ss3(rest),
        // DCS
        0x50 => begin_dcs(rest),
        // OSC
        0x5d => begin_osc(rest),
        // CSI
        0x5b => begin_csi(rest, false),
        _ => Err(ParseError::Incomplete),
    }
}

type CsiArgs = ArrayVec<[Option<NonZeroU32>; 3]>;

fn parse_csi_sequence(mut input: &[u8]) -> ParseResult<'_, (CsiArgs, u8, u16)> {
    let aux_lo = match input.get(0) {
        Some(&c) if c >= b'<' && c <= b'?' => {
            input = &input[1..];
            c
        }
        _ => 0,
    };

    let end_pos = match input
        .iter()
        .position(|&c| c == b'$' || c >= 0x40 && c <= 0x80)
    {
        Some(v) => v,
        None => return Err(ParseError::Incomplete),
    };

    let command = input[end_pos];
    let input = &input[..end_pos];
    let rest = &input[(end_pos + 1).min(input.len())..];

    let mut aux = u16::from(aux_lo);
    let mut arguments = CsiArgs::new();
    let mut found_digit = false;

    for &c in input {
        match c {
            c if c >= b'0' && c <= b'9' => {
                if found_digit {
                    let last = arguments.last_mut().unwrap();
                    *last = last
                        .map_or(0, |a| a.get())
                        .checked_mul(10)
                        .and_then(|a| a.checked_add(u32::from(c - b'0')))
                        .and_then(NonZeroU32::new)
                } else {
                    // ignore overflow
                    let _ = arguments.try_push(NonZeroU32::new(u32::from(c - b'0')));
                    found_digit = true;
                }
            }

            b';' => {
                if !found_digit {
                    // ignore overflow
                    let _ = arguments.try_push(None);
                }

                found_digit = false;
            }

            c if c >= 0x20 && c <= 0x2F => {
                aux |= u16::from(c) << 8;
                break;
            }

            _ => {}
        }
    }

    Ok((rest, (arguments, command, aux)))
}

fn begin_ss3(input: &[u8]) -> ParseResult<'_, Input> {
    begin_csi(input, true)
}

fn begin_dcs(_input: &[u8]) -> ParseResult<'_, Input> {
    unimplemented!()
}

fn begin_osc(_input: &[u8]) -> ParseResult<'_, Input> {
    unimplemented!()
}

fn begin_csi(input: &[u8], is_ss3: bool) -> ParseResult<'_, Input> {
    let (rest, (args, command, aux)) = parse_csi_sequence(input)?;
    match match_csi(args, command, aux, is_ss3) {
        Some(key) => Ok((rest, key)),
        None => Err(ParseError::UnknownSequence),
    }
}

fn match_csi_special(num: u32, mut mods: Modifiers, char: Option<char>) -> Option<Input> {
    if num > 24 && num != 27 {
        mods |= Modifiers::SHIFT; // urxvt
    }

    let key = match num {
        1 => Key::Find,
        2 => Key::Insert,
        3 => Key::Delete,
        4 => Key::Select,
        5 => Key::PageUp,
        6 => Key::PageDown,
        7 => Key::Home,
        8 => Key::End,

        11 => Key::Function(1),
        12 => Key::Function(2),
        13 => Key::Function(3),
        14 => Key::Function(4),
        15 => Key::Function(5),
        17 => Key::Function(6),
        18 => Key::Function(7),
        19 => Key::Function(8),
        20 => Key::Function(9),
        21 => Key::Function(10),
        23 => Key::Function(11),
        24 => Key::Function(12),

        25 => Key::Function(3),
        26 => Key::Function(4),
        28 => Key::Function(5),
        29 => Key::Function(6),
        31 => Key::Function(7),
        32 => Key::Function(8),
        33 => Key::Function(9),
        34 => Key::Function(10),

        // CSI 27 ; modifiers ; codepoint ~
        27 => match char {
            Some(c) => Key::Char(c),
            _ => return None,
        },

        _ => return None,
    };

    Some((key, mods).into())
}

fn get_num(args: &CsiArgs, idx: u8) -> Option<u32> {
    args.get(idx as usize).and_then(|&a| a).map(|a| a.get())
}

fn get_char(args: &[Option<NonZeroU32>], idx: u8) -> Option<char> {
    args.get(idx as usize)
        .and_then(|&a| a)
        .and_then(|a| char::from_u32(a.get()))
}

fn match_csi(args: CsiArgs, command: u8, aux: u16, is_ss3: bool) -> Option<Input> {
    if aux != 0 {
        return None;
    }

    let mods_idx = match command {
        b'~' | b'^' | b'$' | b'@' => 1,
        _ if args.len() > 1 => 1,
        _ => 0,
    };

    let mut mods = args
        .get(mods_idx)
        .and_then(|&a| a)
        .map_or(Modifiers::EMPTY, |a| Modifiers::from_csi(a.get() as u8));

    match command {
        b'^' => mods |= Modifiers::CTRL,
        b'Z' | b'$' => mods |= Modifiers::SHIFT,
        b'a' | b'b' | b'c' | b'd' => {
            mods |= if is_ss3 {
                Modifiers::CTRL
            } else {
                Modifiers::SHIFT
            }
        }
        b'@' => mods |= Modifiers::CTRL | Modifiers::SHIFT,
        _ => (),
    }

    let key = match command {
        // unicode keys: CSI codepoint ; modifiers u
        b'u' if args.len() > 1 => return get_char(&args, 0).map(|c| (Key::Char(c), mods).into()),

        // special keys: CSI number ; modifiers ~
        b'~' | b'^' | b'$' | b'@' if !args.is_empty() => {
            return match_csi_special(get_num(&args, 0).unwrap_or(0), mods, get_char(&args, 2))
        }

        // very special keys: CSI 1 ; modifiers <>
        b'A' => Key::Up,
        b'B' => Key::Down,
        b'C' => Key::Right,
        b'D' => Key::Left,
        b'E' => Key::Begin,
        b'F' => Key::End,
        b'H' => Key::Home,
        b'P' => Key::Function(1),
        b'Q' => Key::Function(2),
        b'R' => Key::Function(3),
        b'S' => Key::Function(4),

        b'a' => Key::Up,
        b'b' => Key::Down,
        b'c' => Key::Right,
        b'd' => Key::Left,

        b'Z' => Key::Tab,

        _ => return None,
    };

    Some((key, mods).into())
}

pub fn parse(input: &[u8]) -> ParseResult<'_, Input> {
    if input.is_empty() {
        return Err(ParseError::Incomplete);
    }

    let rest = &input[1..];

    match input[0] {
        // ESC
        0x1B => begin_escape_prefixed(rest),
        // SS3 (C1)
        0x8F => begin_ss3(rest),
        // DCS (C1)
        0x90 => begin_dcs(rest),
        // OSC (C1)
        0x9D => begin_osc(rest),
        // CSI (C1)
        0x9B => begin_csi(rest, false),
        _ => Err(ParseError::UnknownSequence),
    }
}
