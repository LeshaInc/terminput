use terminput::{parse, Key, Modifiers};

macro_rules! assert_key {
    ($input:expr, $key:expr) => {
        assert_eq!(parse($input), Ok((&b""[..], $key.into())));
    };
}

#[test]
fn simple() {
    // urxvt
    assert_key!(b"\x1B[11~", Key::Function(1));
    assert_key!(b"\x1B[12~", Key::Function(2));
    assert_key!(b"\x1B[13~", Key::Function(3));
    assert_key!(b"\x1B[14~", Key::Function(4));
    assert_key!(b"\x1B[15~", Key::Function(5));
    assert_key!(b"\x1B[17~", Key::Function(6));
    assert_key!(b"\x1B[18~", Key::Function(7));
    assert_key!(b"\x1B[19~", Key::Function(8));
    assert_key!(b"\x1B[20~", Key::Function(9));
    assert_key!(b"\x1B[21~", Key::Function(10));
    assert_key!(b"\x1B[23~", Key::Function(11));
    assert_key!(b"\x1B[24~", Key::Function(12));

    // xterm, alacritty, konsole
    assert_key!(b"\x1BOP", Key::Function(1));
    assert_key!(b"\x1BOQ", Key::Function(2));
    assert_key!(b"\x1BOR", Key::Function(3));
    assert_key!(b"\x1BOS", Key::Function(4));
}

#[test]
fn ctrl() {
    // urxvt
    assert_key!(b"\x1B[11^", (Key::Function(1), Modifiers::CTRL));
    assert_key!(b"\x1B[12^", (Key::Function(2), Modifiers::CTRL));
    assert_key!(b"\x1B[13^", (Key::Function(3), Modifiers::CTRL));
    assert_key!(b"\x1B[14^", (Key::Function(4), Modifiers::CTRL));
    assert_key!(b"\x1B[15^", (Key::Function(5), Modifiers::CTRL));
    assert_key!(b"\x1B[17^", (Key::Function(6), Modifiers::CTRL));
    assert_key!(b"\x1B[18^", (Key::Function(7), Modifiers::CTRL));
    assert_key!(b"\x1B[19^", (Key::Function(8), Modifiers::CTRL));
    assert_key!(b"\x1B[20^", (Key::Function(9), Modifiers::CTRL));
    assert_key!(b"\x1B[21^", (Key::Function(10), Modifiers::CTRL));
    assert_key!(b"\x1B[23^", (Key::Function(11), Modifiers::CTRL));
    assert_key!(b"\x1B[24^", (Key::Function(12), Modifiers::CTRL));

    // xterm, alacritty
    assert_key!(b"\x1B[1;5P", (Key::Function(1), Modifiers::CTRL));
    assert_key!(b"\x1B[1;5Q", (Key::Function(2), Modifiers::CTRL));
    assert_key!(b"\x1B[1;5R", (Key::Function(3), Modifiers::CTRL));
    assert_key!(b"\x1B[1;5S", (Key::Function(4), Modifiers::CTRL));
    assert_key!(b"\x1B[15;5~", (Key::Function(5), Modifiers::CTRL));
    assert_key!(b"\x1B[17;5~", (Key::Function(6), Modifiers::CTRL));
    assert_key!(b"\x1B[18;5~", (Key::Function(7), Modifiers::CTRL));
    assert_key!(b"\x1B[19;5~", (Key::Function(8), Modifiers::CTRL));
    assert_key!(b"\x1B[20;5~", (Key::Function(9), Modifiers::CTRL));
    assert_key!(b"\x1B[21;5~", (Key::Function(10), Modifiers::CTRL));
    assert_key!(b"\x1B[23;5~", (Key::Function(11), Modifiers::CTRL));
    assert_key!(b"\x1B[24;5~", (Key::Function(12), Modifiers::CTRL));

    // konsole
    assert_key!(b"\x1BO5P", (Key::Function(1), Modifiers::CTRL));
    assert_key!(b"\x1BO5Q", (Key::Function(2), Modifiers::CTRL));
    assert_key!(b"\x1BO5R", (Key::Function(3), Modifiers::CTRL));
    assert_key!(b"\x1BO5S", (Key::Function(4), Modifiers::CTRL));
}

#[test]
fn shift() {
    // urxvt
    assert_key!(b"\x1B[25~", (Key::Function(3), Modifiers::SHIFT));
    assert_key!(b"\x1B[26~", (Key::Function(4), Modifiers::SHIFT));
    assert_key!(b"\x1B[28~", (Key::Function(5), Modifiers::SHIFT));
    assert_key!(b"\x1B[29~", (Key::Function(6), Modifiers::SHIFT));
    assert_key!(b"\x1B[31~", (Key::Function(7), Modifiers::SHIFT));
    assert_key!(b"\x1B[32~", (Key::Function(8), Modifiers::SHIFT));
    assert_key!(b"\x1B[33~", (Key::Function(9), Modifiers::SHIFT));
    assert_key!(b"\x1B[34~", (Key::Function(10), Modifiers::SHIFT));
    assert_key!(b"\x1B[23$", (Key::Function(11), Modifiers::SHIFT));
    assert_key!(b"\x1B[24$", (Key::Function(12), Modifiers::SHIFT));

    // xterm, alacritty
    assert_key!(b"\x1B[1;2P", (Key::Function(1), Modifiers::SHIFT));
    assert_key!(b"\x1B[1;2Q", (Key::Function(2), Modifiers::SHIFT));
    assert_key!(b"\x1B[1;2R", (Key::Function(3), Modifiers::SHIFT));
    assert_key!(b"\x1B[1;2S", (Key::Function(4), Modifiers::SHIFT));
    assert_key!(b"\x1B[15;2~", (Key::Function(5), Modifiers::SHIFT));
    assert_key!(b"\x1B[17;2~", (Key::Function(6), Modifiers::SHIFT));
    assert_key!(b"\x1B[18;2~", (Key::Function(7), Modifiers::SHIFT));
    assert_key!(b"\x1B[19;2~", (Key::Function(8), Modifiers::SHIFT));
    assert_key!(b"\x1B[20;2~", (Key::Function(9), Modifiers::SHIFT));
    assert_key!(b"\x1B[21;2~", (Key::Function(10), Modifiers::SHIFT));
    assert_key!(b"\x1B[23;2~", (Key::Function(11), Modifiers::SHIFT));
    assert_key!(b"\x1B[24;2~", (Key::Function(12), Modifiers::SHIFT));

    // konsole
    assert_key!(b"\x1BO2P", (Key::Function(1), Modifiers::SHIFT));
    assert_key!(b"\x1BO2Q", (Key::Function(2), Modifiers::SHIFT));
    assert_key!(b"\x1BO2R", (Key::Function(3), Modifiers::SHIFT));
    assert_key!(b"\x1BO2S", (Key::Function(4), Modifiers::SHIFT));
}

#[test]
fn ctrl_shift() {
    let modifiers = Modifiers::CTRL | Modifiers::SHIFT;

    // urxvt
    assert_key!(b"\x1B[25^", (Key::Function(3), modifiers));
    assert_key!(b"\x1B[26^", (Key::Function(4), modifiers));
    assert_key!(b"\x1B[28^", (Key::Function(5), modifiers));
    assert_key!(b"\x1B[29^", (Key::Function(6), modifiers));
    assert_key!(b"\x1B[31^", (Key::Function(7), modifiers));
    assert_key!(b"\x1B[32^", (Key::Function(8), modifiers));
    assert_key!(b"\x1B[33^", (Key::Function(9), modifiers));
    assert_key!(b"\x1B[34^", (Key::Function(10), modifiers));
    assert_key!(b"\x1B[23@", (Key::Function(11), modifiers));
    assert_key!(b"\x1B[24@", (Key::Function(12), modifiers));

    // xterm, alacritty
    assert_key!(b"\x1B[1;6P", (Key::Function(1), modifiers));
    assert_key!(b"\x1B[1;6Q", (Key::Function(2), modifiers));
    assert_key!(b"\x1B[1;6R", (Key::Function(3), modifiers));
    assert_key!(b"\x1B[1;6S", (Key::Function(4), modifiers));
    assert_key!(b"\x1B[15;6~", (Key::Function(5), modifiers));
    assert_key!(b"\x1B[17;6~", (Key::Function(6), modifiers));
    assert_key!(b"\x1B[18;6~", (Key::Function(7), modifiers));
    assert_key!(b"\x1B[19;6~", (Key::Function(8), modifiers));
    assert_key!(b"\x1B[20;6~", (Key::Function(9), modifiers));
    assert_key!(b"\x1B[21;6~", (Key::Function(10), modifiers));
    assert_key!(b"\x1B[23;6~", (Key::Function(11), modifiers));
    assert_key!(b"\x1B[24;6~", (Key::Function(12), modifiers));

    // konsole
    assert_key!(b"\x1BO6P", (Key::Function(1), modifiers));
    assert_key!(b"\x1BO6Q", (Key::Function(2), modifiers));
    assert_key!(b"\x1BO6R", (Key::Function(3), modifiers));
    assert_key!(b"\x1BO6S", (Key::Function(4), modifiers));
}
