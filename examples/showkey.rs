use std::io::{stdin, stdout, Read, Result, Write};
use termion::raw::IntoRawMode;

use terminput::{parse, Key, Modifiers, ParseError};

fn main() -> Result<()> {
    let mut stdout = stdout().into_raw_mode()?;

    let mut buf = [0; 128];
    let mut length = 0;

    writeln!(stdout, "press ctrl-c to exit\r")?;

    loop {
        length += stdin().read(&mut buf[length..])?;

        match parse(&buf[..length]) {
            Ok((_, key)) => {
                writeln!(stdout, "{}\r", key)?;

                if key == (Key::Char('c'), Modifiers::CTRL).into() {
                    break Ok(());
                }

                length = 0;
            }

            Err(ParseError::Incomplete) => (),

            _ => {
                writeln!(stdout, "invalid sequence\r")?;
                length = 0;
            }
        }

        stdout.flush()?;
    }
}
