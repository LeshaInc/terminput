# terminput

A small library focused on parsing terminal input sequences (keyboard, mouse, terminal size responses, and so on).

## Why not...

 - ...[ncurses]? It is a huge library which handles not only the keyboard input but also graphics and managing
   terminal properties. And it still doesn't really help to distinguish such combinations as alt+arrows, ctrl+f1..12,
   and many others, in a simple, cross-terminal way.

 - ...[libtermkey]? It is based on the same idea as this library, even more: terminput-rs has been started with
   rewriting [libtermkey] in safe, idiomatic Rust. However, [libtermkey] is deprecated, and its author encourages us
   to use [libtickit], which is not what simple programs need...

 - ...[libtickit]? It is even more complicated than [ncurses]. And besides that, you have to setup a bunch of event
   handlers just to read some keys. Push-style APIs are generally much worse than pull-style APIs, especially
   across language boundaries.

 - ...hardcoded byte sequences? Many terminals send different byte sequences for same key combinations. For example,
   for F1 key urxvt sends `ESC [ 1 1 ~`, while alacritty sends `ESC O P`. Things become much worse when you consider
   key modifiers such as alt, control, and shift: for Shift-F1 urxvt sends `ESC [ 2 3 ~`, alacritty sends
   `ESC [ 1 ; 2 P`, while xterm sends `ESC [ 11 ; 2 ~`.

[ncurses]: https://www.gnu.org/software/ncurses/ncurses.html
[libtermkey]: http://www.leonerd.org.uk/code/libtermkey/
[libtickit]: http://www.leonerd.org.uk/code/libtickit/

## License

This project is licensed like the Rust language itself under either of

 - Apache License, Version 2.0 (see the `LICENSE-APACHE` file
   or http://www.apache.org/licenses/LICENSE-2.0)
 - MIT license (see the `LICENSE-MIT` file
   or https://opensource.org/licenses/MIT)

at your option.
